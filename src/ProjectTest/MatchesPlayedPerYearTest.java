package ProjectTest;
import Projects.Main;
import org.junit.AfterClass;
import org.junit.Test;

import static org.junit.Assert.*;

public class MatchesPlayedPerYearTest {

    @Test
    public void wrongPathException(){
        Main classOfMatchesPlayedPerYear = new Main();

        String actualResult = classOfMatchesPlayedPerYear.eachSeasonCount("src/matchs.csv");

        String expectedResult = "File Not Found";

        assertEquals("Path File Not Found Assertion", expectedResult,actualResult);

    }
    @Test
    public void assertFalseForException(){
        Main classOfMatchesPlayedPerYear = new Main();

        String actualResult = classOfMatchesPlayedPerYear.eachSeasonCount("src/matches.csv");

        String expectedResult = "File Not Found";

        assertFalse("Assert False Exception For Correct Path", expectedResult == actualResult);

    }

    @Test
    public void nullException(){
        Main classOfMatchesPlayedPerYear = new Main();

        String actualResult = classOfMatchesPlayedPerYear.eachSeasonCount(null);

        assertNull("Passing Null",actualResult);
    }

    @Test
    public void notNullException(){
        Main classOfMatchesPlayedPerYear = new Main();

        String actualResult = classOfMatchesPlayedPerYear.eachSeasonCount("src/MatchData/matches.csv");

        assertNotNull("Checking Result Not Null", actualResult);
    }

    @Test
    public void returnType(){
        Main classOfMatchesPlayedPerYear = new Main();

        String actualResult = classOfMatchesPlayedPerYear.eachSeasonCount("src/matches.csv");

        String expectedResult = "String";

        assertEquals("Checking Return Type", expectedResult,actualResult.getClass().getSimpleName());
    }

    @Test
    public void assertEqualsOfMatchesPlayedPerYear(){
        Main classOfMatchesPlayedPerYear = new Main();

        String actualResult = classOfMatchesPlayedPerYear.eachSeasonCount("src/matches.csv");

        String expectedResult = "{2008=58, 2009=57, 2010=60, 2011=73, 2012=74, 2013=76, 2014=60, 2015=59, 2016=60, 2017=59}";

        assertEquals("Checking Final Result", expectedResult,actualResult);
    }

    @AfterClass
    public static void finalMessageOfTest(){
        System.out.println("Passed All TestCases in MatchesPlayedPerYear");
    }


}
