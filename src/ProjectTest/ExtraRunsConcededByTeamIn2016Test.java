package ProjectTest;
import Projects.Main;
import org.junit.AfterClass;
import org.junit.Test;

import static org.junit.Assert.*;

public class ExtraRunsConcededByTeamIn2016Test {

    String matchDataPath = "src/matches.csv";
    String deliveryDataPath = "src/deliveries.csv";
    String year = "2016";

    @Test
    public void wrongPathException(){
        Main classOfExtraRunsConcededByTeamIn2016 = new Main();

        String actualResult = classOfExtraRunsConcededByTeamIn2016.extraRunsConcededByTeamInYear("src/matchs.csv",deliveryDataPath,year);

        String expectedResult = "File Not Found";

        assertEquals("Path File Not Found Assertion", expectedResult,actualResult);

    }


    @Test
    public void assertFalseForException(){
        Main classOfExtraRunsConcededByTeamIn2016 = new Main();

        String actualResult = classOfExtraRunsConcededByTeamIn2016.extraRunsConcededByTeamInYear(matchDataPath,deliveryDataPath,year);

        String expectedResult = "File Not Found";

        assertFalse("Assert False Exception For Correct Path", expectedResult == actualResult);

    }

    @Test
    public void nullException(){
        Main classOfExtraRunsConcededByTeamIn2016 = new Main();

        String actualResult = classOfExtraRunsConcededByTeamIn2016.extraRunsConcededByTeamInYear(matchDataPath,null,null);

        assertNull("Passing Null",actualResult);
    }

    @Test
    public void notNullException(){
        Main classOfExtraRunsConcededByTeamIn2016 = new Main();

        String actualResult = classOfExtraRunsConcededByTeamIn2016.extraRunsConcededByTeamInYear(matchDataPath,deliveryDataPath,year);

        assertNotNull("Checking Result Not Null", actualResult);
    }

    @Test
    public void returnType(){
        Main classOfExtraRunsConcededByTeamIn2016 = new Main();

        String actualResult = classOfExtraRunsConcededByTeamIn2016.extraRunsConcededByTeamInYear(matchDataPath,deliveryDataPath,year);

        String expectedResult = "String";

        assertEquals("Checking Return Type", expectedResult,actualResult.getClass().getSimpleName());
    }

    @Test
    public void assertEqualsOfExtraRunsConcededByTeamIn2016(){
        Main classOfExtraRunsConcededByTeamIn2016 = new Main();

        String actualResult = classOfExtraRunsConcededByTeamIn2016.extraRunsConcededByTeamInYear(matchDataPath,deliveryDataPath,year);

        String expectedResult = "{Delhi Daredevils=106, Gujarat Lions=98, Kings XI Punjab=100, Kolkata Knight Riders=122, Mumbai Indians=102, Rising Pune Supergiants=108, Royal Challengers Bangalore=156, Sunrisers Hyderabad=107}";

        assertEquals("Checking Final Result", expectedResult,actualResult);
    }

    @AfterClass
    public static void finalMessageOfTest(){
        System.out.println("Passed All TestCases in ExtraRunsConcededByTeamIn2016");
    }
}

