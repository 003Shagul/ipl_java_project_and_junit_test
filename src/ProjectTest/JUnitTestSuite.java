package ProjectTest;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;


// A test suite bundles a few unit test cases and runs them together.
// In JUnit, both @RunWith and @Suite annotation are used to run the suite test.


@RunWith(Suite.class)
@Suite.SuiteClasses({
        MatchesPlayedPerYearTest.class,
        MatchesWonByTeamPerYearTest.class,
        ExtraRunsConcededByTeamIn2016Test.class,
        TopEconomicalBowlersIn2015Test.class
})

public class JUnitTestSuite {

    @BeforeClass
    public static void printMe(){
        System.out.println("Running Test Suite");
    }

    @AfterClass
    public static void printMeAfterExecution() {
        System.out.println("Ran All The Test Cases");
    }
}
