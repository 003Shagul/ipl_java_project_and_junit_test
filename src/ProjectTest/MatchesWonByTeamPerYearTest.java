package ProjectTest;

import Projects.Main;
import org.junit.AfterClass;
import org.junit.Test;

import static org.junit.Assert.*;

public class MatchesWonByTeamPerYearTest {

    @Test
    public void wrongPathException(){
        Main classOfMatchesWonByTeamPerYear = new Main();

        String actualResult = classOfMatchesWonByTeamPerYear.matchWonByTeam("src/matchs.csv");

        String expectedResult = "File Not Found";

        assertEquals("Path File Not Found Assertion", expectedResult,actualResult);

    }

    @Test
    public void assertFalseForException(){
        Main classOfMatchesWonByTeamPerYear = new Main();

        String actualResult = classOfMatchesWonByTeamPerYear.matchWonByTeam("src/matches.csv");

        String expectedResult = "File Not Found";

        assertFalse("Assert False Exception For Correct Path", expectedResult == actualResult);

    }

    @Test
    public void nullException(){
        Main classOfMatchesWonByTeamPerYear = new Main();

        String actualResult = classOfMatchesWonByTeamPerYear.matchWonByTeam(null);

        assertNull("Passing Null",actualResult);
    }

    @Test
    public void notNullException(){
        Main classOfMatchesWonByTeamPerYear = new Main();

        String actualResult = classOfMatchesWonByTeamPerYear.matchWonByTeam("src/matches.csv");

        assertNotNull("Checking Result Not Null", actualResult);
    }

    @Test
    public void returnType(){
        Main classOfMatchesWonByTeamPerYear = new Main();

        String actualResult = classOfMatchesWonByTeamPerYear.matchWonByTeam("src/matches.csv");

        String expectedResult = "String";

        assertEquals("Checking Return Type", expectedResult,actualResult.getClass().getSimpleName());
    }

    @Test
    public void assertEqualsOfMatchesWonPerYear(){
        Main classOfMatchesWonByTeamPerYear = new Main();

        String actualResult = classOfMatchesWonByTeamPerYear.matchWonByTeam("src/matches.csv");

        String expectedResult = "{Chennai Super Kings={2008=9, 2009=8, 2010=9, 2011=11, 2012=10, 2013=12, 2014=10, 2015=10}, Deccan Chargers={2008=2, 2009=9, 2010=8, 2011=6, 2012=4}, Delhi Daredevils={2008=7, 2009=10, 2010=7, 2011=4, 2012=11, 2013=3, 2014=2, 2015=5, 2016=7, 2017=6}, Gujarat Lions={2016=9, 2017=4}, Kings XI Punjab={2008=10, 2009=7, 2010=4, 2011=7, 2012=8, 2013=8, 2014=12, 2015=3, 2016=4, 2017=7}, Kochi Tuskers Kerala={2011=6}, Kolkata Knight Riders={2008=6, 2009=3, 2010=7, 2011=8, 2012=12, 2013=6, 2014=11, 2015=7, 2016=8, 2017=9}, Mumbai Indians={2008=7, 2009=5, 2010=11, 2011=10, 2012=10, 2013=13, 2014=7, 2015=10, 2016=7, 2017=12}, No result={2011=1, 2015=2}, Pune Warriors={2011=4, 2012=4, 2013=4}, Rajasthan Royals={2008=13, 2009=6, 2010=6, 2011=6, 2012=7, 2013=11, 2014=7, 2015=7}, Rising Pune Supergiant={2017=10}, Rising Pune Supergiants={2016=5}, Royal Challengers Bangalore={2008=4, 2009=9, 2010=8, 2011=10, 2012=8, 2013=9, 2014=5, 2015=8, 2016=9, 2017=3}, Sunrisers Hyderabad={2013=10, 2014=6, 2015=7, 2016=11, 2017=8}}";

        assertEquals("Checking Final Result", expectedResult,actualResult);
    }

    @AfterClass
    public static void finalMessageOfTest(){
        System.out.println("Passed All TestCases in MatchesWonByTeamPerYear");
    }

}
