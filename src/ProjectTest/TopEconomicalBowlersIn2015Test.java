package ProjectTest;

import Projects.Main;
import org.junit.AfterClass;
import org.junit.Test;

import static org.junit.Assert.*;

public class TopEconomicalBowlersIn2015Test {

    String matchDataPath = "src/matches.csv";
    String deliveryDataPath = "src/deliveries.csv";
    String year = "2015";

    @Test
    public void wrongPathException(){
        Main classOfTopEconomicalBowlersIn2015 = new Main();

        String actualResult = classOfTopEconomicalBowlersIn2015.topEconomicalBowlersIn2015("src/matchs.csv",deliveryDataPath,year);

        String expectedResult = "File Not Found";

        assertEquals("Path File Not Found Assertion", expectedResult,actualResult);

    }
    @Test
    public void assertFalseForException(){
        Main classOfTopEconomicalBowlersIn2015 = new Main();

        String actualResult = classOfTopEconomicalBowlersIn2015.topEconomicalBowlersIn2015(matchDataPath,deliveryDataPath,year);

        String expectedResult = "File Not Found";

        assertFalse("Assert False Exception For Correct Path", expectedResult == actualResult);

    }

    @Test
    public void nullException(){
        Main classOfTopEconomicalBowlersIn2015 = new Main();

        String actualResult = classOfTopEconomicalBowlersIn2015.topEconomicalBowlersIn2015(null,deliveryDataPath,year);

        assertNull("Passing Null",actualResult);
    }

    @Test
    public void notNullException(){
        Main classOfTopEconomicalBowlersIn2015 = new Main();

        String actualResult = classOfTopEconomicalBowlersIn2015.topEconomicalBowlersIn2015(matchDataPath,deliveryDataPath,year);

        assertNotNull("Checking Result Not Null", actualResult);
    }

    @Test
    public void returnType(){
        Main classOfTopEconomicalBowlersIn2015 = new Main();

        String actualResult = classOfTopEconomicalBowlersIn2015.topEconomicalBowlersIn2015(matchDataPath,deliveryDataPath,year);

        String expectedResult = "String";

        assertEquals("Checking Return Type", expectedResult,actualResult.getClass().getSimpleName());
    }
    @AfterClass
    public static void finalMessageOfTest(){
        System.out.println("Passed All TestCases in Top10EconomicalBowlersIn2015");
    }

}
