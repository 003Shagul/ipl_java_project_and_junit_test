package Projects;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
public class Main {
    public static void main(String[] args) {
        String matchDataPath = "src/matches.csv";
        String deliveryDataPath = "src/deliveries.csv";
        String year = "2015";
        String season = "2016";


        System.out.println(eachSeasonCount(matchDataPath));
        System.out.println(matchWonByTeam(matchDataPath));
        System.out.println(extraRunsConcededByTeamInYear(matchDataPath,deliveryDataPath,season));
        System.out.println(topEconomicalBowlersIn2015(matchDataPath,deliveryDataPath,year));

    }
    public static String eachSeasonCount(String matchDataPath) {
        String EachLine = "";
        Map<String, Integer> matchesPlayedPerYear = new TreeMap<>();


        try {
            BufferedReader br = new BufferedReader(new FileReader(matchDataPath));

            br.readLine();

            while ((EachLine = br.readLine()) != null) {
                String[] splittedLine = EachLine.split(",");
                String season = splittedLine[1];

                if (!(matchesPlayedPerYear.containsKey(season))) {
                    matchesPlayedPerYear.put(season, 0);
                }

                if (matchesPlayedPerYear.containsKey(season)) {
                    int count = matchesPlayedPerYear.get(season) + 1;
                    matchesPlayedPerYear.replace(season, count);
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
//        } catch (IOException e) {
//            throw new RuntimeException(e);
//        }
//        catch (ArrayIndexOutOfBoundsException e){
//            return "Index Out Of Range";
//        }
//        catch (NullPointerException e) {
//            return null ;
        }
        return matchesPlayedPerYear.toString();
    }
    public static String matchWonByTeam(String matchDataPath){

        String EachLine = "";
        Map<String, TreeMap<String, Integer>> matchesWonByTeamPerYear = new TreeMap<>();


        try {
            BufferedReader br = new BufferedReader(new FileReader(matchDataPath));
            br.readLine();

            while ((EachLine = br.readLine()) != null) {
                String[] splittedLine = EachLine.split(",");
                String winningTeamName = splittedLine[10];
                String season = (splittedLine[1]);

                if (winningTeamName == "") {
                    winningTeamName = "No result";
                }

                if (!(matchesWonByTeamPerYear.containsKey(winningTeamName))) {
                    matchesWonByTeamPerYear.put(winningTeamName, new TreeMap<>());
                }
                if ((matchesWonByTeamPerYear.containsKey(winningTeamName))) {
                    if (!((matchesWonByTeamPerYear.get( winningTeamName )).containsKey(season))){
                        matchesWonByTeamPerYear.get(winningTeamName).put(season, 0);
                    }
                    if ((matchesWonByTeamPerYear.get( winningTeamName )).containsKey(season)){
                        int increaseCount = matchesWonByTeamPerYear.get(winningTeamName).get(season) + 1;
                        matchesWonByTeamPerYear.get(winningTeamName).put(season, increaseCount);
                    }
                }

            }

        } catch (FileNotFoundException e) {
            return "File Not Found";
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        catch (ArrayIndexOutOfBoundsException e){
            return "Index Out Of Range";
        }
        catch (NullPointerException e) {
            return null ;
        }

        return matchesWonByTeamPerYear.toString();

    }
    public static String extraRunsConcededByTeamInYear(String matchDataPath, String deliveryDataPath,String year) {


        String eachLine = "";
        List allMatchIdIn2016 = new ArrayList();
        Map<String, Integer> extraRunsConcededByTeamIn2016 = new TreeMap<>();

        try {
            BufferedReader allMatchData = new BufferedReader(new  FileReader(matchDataPath));

            allMatchData.readLine();

            while ((eachLine = allMatchData.readLine()) != null ) {
                String[] splittedData = eachLine.split(",");
                String matchId = splittedData[0];
                String season = splittedData[1];

                if (season.equals(year)){
                    allMatchIdIn2016.add(matchId);
                }

            }


        } catch (FileNotFoundException e) {
            return "File Not Found";
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        catch (ArrayIndexOutOfBoundsException e){
            return "Index Out Of Range";
        }
        catch (NullPointerException e) {
            return null ;
        }

        try {
            BufferedReader allDeliveryData = new BufferedReader(new FileReader(deliveryDataPath)) ;
            String eachDeliveryLine = "";

            allDeliveryData.readLine();

            while ((eachDeliveryLine = allDeliveryData.readLine()) != null) {
                String[] splittedDeliveryData =  eachDeliveryLine.split(",");

                String deliveryMatchId = splittedDeliveryData[0];
                String bowlingTeam = splittedDeliveryData[3];
                int extraRuns = Integer.parseInt(splittedDeliveryData[16]);

                if (allMatchIdIn2016.contains(deliveryMatchId)) {

                    if (!(extraRunsConcededByTeamIn2016.containsKey(bowlingTeam))) {
                        extraRunsConcededByTeamIn2016.put(bowlingTeam, 0);
                    }

                    if (extraRunsConcededByTeamIn2016.containsKey(bowlingTeam)) {
                        int addedExtraRuns = extraRunsConcededByTeamIn2016.get(bowlingTeam) + extraRuns;
                        extraRunsConcededByTeamIn2016.replace(bowlingTeam, addedExtraRuns);
                    }
                }
            }
        } catch (FileNotFoundException e) {
            return "File Not Found";
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        catch (ArrayIndexOutOfBoundsException e){
            return "Index Out Of Range";
        }
        catch (NullPointerException e) {
            return null ;
        }

        return extraRunsConcededByTeamIn2016.toString();
    }
    public static String topEconomicalBowlersIn2015(String matchDataPath, String deliveryDataPath,String year) {

        String eachMatchLine = "";
        List allMatchIdIn2015 = new ArrayList();
        Map<String, TreeMap<String, Integer>> topEconomicalBowlersIn2015 = new TreeMap<>();

        try {
            BufferedReader allMatchData = new BufferedReader(new FileReader(matchDataPath));

            allMatchData.readLine();

            while ((eachMatchLine = allMatchData.readLine()) != null ) {
                String[] splittedData = eachMatchLine.split(",");
                String matchId = splittedData[0];
                String season = splittedData[1];

                if (season.equals("2015")){
                    allMatchIdIn2015.add(matchId);
                }

            }


        } catch (FileNotFoundException e) {
            return "File Not Found";
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        catch (ArrayIndexOutOfBoundsException e){
            return "Index Out Of Range";
        }
        catch (NullPointerException e) {
            return null ;
        }

        try {
            BufferedReader allDeliveryData = new BufferedReader(new FileReader(deliveryDataPath)) ;
            String eachDeliveryLine = "";

            allDeliveryData.readLine();

            while ((eachDeliveryLine = allDeliveryData.readLine()) != null) {
                String[] splittedDeliveryData =  eachDeliveryLine.split(",");

                String deliveryMatchId = splittedDeliveryData[0];
                String bowlerName = splittedDeliveryData[8];

                int totalRuns = Integer.parseInt(splittedDeliveryData[17]);

                if (allMatchIdIn2015.contains(deliveryMatchId)) {

                    if (!(topEconomicalBowlersIn2015.containsKey(bowlerName))) {
                        topEconomicalBowlersIn2015.put(bowlerName, new TreeMap<>());
                        topEconomicalBowlersIn2015.get(bowlerName).put("balls",0);
                        topEconomicalBowlersIn2015.get(bowlerName).put("runsConceded",0);
                    }
                    if(topEconomicalBowlersIn2015.containsKey(bowlerName)){
                        int increaseBallCount = topEconomicalBowlersIn2015.get(bowlerName).get("balls") + 1;
                        int increaseConcededRuns = topEconomicalBowlersIn2015.get(bowlerName).get("runsConceded")+totalRuns;
                        topEconomicalBowlersIn2015.get(bowlerName).replace("balls",increaseBallCount);
                        topEconomicalBowlersIn2015.get(bowlerName).replace("runsConceded",increaseConcededRuns);
                    }
                }
            }
        } catch (FileNotFoundException e) {
            return "File Not Found";
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        catch (ArrayIndexOutOfBoundsException e){
            return "Index Out Of Range";
        }
        catch (NullPointerException e) {
            return null ;
        }

        Map<String, Double> topEconomyBowlers = new HashMap<>();

        topEconomicalBowlersIn2015.forEach((key,value) -> {
            String bowlerName = key;
            int ballCount = value.get("balls");
            int totalRuns = value.get("runsConceded");
            double oversCount = ballCount/6;
            double economyRate = (totalRuns/oversCount);
            String stringEconomyRate = String.format("%.2f",economyRate);

            topEconomyBowlers.put(bowlerName, Double.parseDouble(stringEconomyRate));
        });


        Set<Map.Entry<String, Double>> entrySetOfTopEconomicalBowlers = topEconomyBowlers.entrySet();

        List<Map.Entry<String, Double>> listOfTopEconomicalBowlers = new ArrayList<>(entrySetOfTopEconomicalBowlers);

        Collections.sort(listOfTopEconomicalBowlers,(o1,o2) -> o1.getValue().compareTo(o2.getValue()));

        List top10EconomicalBowlers = listOfTopEconomicalBowlers.subList(0, 1);


        return top10EconomicalBowlers.toString();
    }
}



